//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a dummy control that consumes space in the layout without drawing anything.
 */
void mgui::dummy() {
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + globals::global_config.item_spacing});
}

/**
 * @brief Advances the layout cursor to the next column.
 * @param pusher_x The horizontal offset to the next column from the current cursor position.
 * @param pusher_y The vertical offset to the next column from the base position.
 */
void mgui::next_column(const int pusher_x, const int pusher_y) {
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 new_cursor_pos{cursor_pos.x + pusher_x, globals::global_config.base_pos.y + pusher_y};

    if (globals::window_ctx.next_cursor_position.y != 0)
        new_cursor_pos.y += 14; 

    utils::misc::push_cursor_pos(new_cursor_pos);
}

/**
 * @brief Positions the next control on the same line as the previous control, with an optional x-axis offset.
 * @param x_axis The x offset from the start of the line. A value of -1 maintains the current x position.
 */
void mgui::same_line(const float x_axis) {
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();

    if (x_axis != -1) {
        utils::misc::push_cursor_pos(Vec2{globals::global_config.base_pos.x + x_axis, cursor_pos.y});
    } else {
        utils::misc::push_cursor_pos(cursor_pos);
    }
}

/**
 * @brief Moves the layout cursor back to the previous line.
 */
void mgui::backup_line() {
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();

    utils::misc::push_cursor_pos(Vec2{globals::window_ctx.position.x, cursor_pos.y - globals::global_config.item_spacing});
}