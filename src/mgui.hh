#ifndef _MINIMAL_GUI_HPP
#define _MINIMAL_GUI_HPP

#include <cstdint>
#include <stack>
#include <vector>
#include <string>
#include <string_view>
#include <type_traits>
#include <windows.h>
#include <algorithm>
#include <iomanip>
#include <sstream>

#define GUI_API __declspec(dllexport)

/**
 * @file minimalgui.h
 * @brief Main header file for the minimal GUI system, defining structures, enums, and functions for GUI operation.
 *
 * This file contains definitions for the GUI system, including types for colors, vectors, and various GUI elements,
 * as well as functions for creating and manipulating these elements.
 */
namespace mgui {

    /**
     * @struct MultiSelectableItem
     * @brief Represents an item that can be selected within a multi-select context.
     */
    struct MultiSelectableItem {
        std::string_view name; // Display name of the item.
        bool* value; // Pointer to the value indicating whether the item is selected.
    };

    /**
     * @struct Vec2
     * @brief Represents a two-dimensional vector.
     */
    struct Vec2 {
        float x, y; // The x and y coordinates of the vector.
    };

    /**
     * @struct Color
     * @brief Represents a color with red, green, blue, and alpha components.
     */
    struct Color {
        int r, g, b, a; // The red, green, blue, and alpha components of the color.
    };

    // Function pointer types for drawing primitives and text.
    using LineFunction = std::add_pointer_t<void(int, int, int, int, Color) noexcept>;
    using RectFunction = std::add_pointer_t<void(int, int, int, int, Color) noexcept>;
    using FilledRectFunction = std::add_pointer_t<void(int, int, int, int, Color) noexcept>;
    using TextFunction = std::add_pointer_t<void(int, int, Color, int, bool, const char*) noexcept>;
    using GetTextSizeFunction = std::add_pointer_t<void(unsigned long, const char*, int&, int&) noexcept>;
    using GetFrameTimeFunction = std::add_pointer_t<float() noexcept>;

    /**
     * @struct Functions
     * @brief Holds function pointers to graphical functions provided by the GUI.
     */
    struct Functions {
        LineFunction draw_line; // Function to draw a line.
        RectFunction draw_rect; // Function to draw a rectangle.
        FilledRectFunction draw_filled_rect; // Function to draw a filled rectangle.
        TextFunction draw_text; // Function to draw text.
        GetTextSizeFunction get_text_size; // Function to get the size of text.
        GetFrameTimeFunction get_frametime; // Function to get the frame time.
    };
    extern Functions functions;

    // WindowFlags defines flags for window appearance and behavior.
    enum WindowFlags {
        WINDOW_FLAG_NONE = 0,
        WINDOW_FLAG_NO_BORDER = 1 << 0,
        WINDOW_FLAG_NO_TITLEBAR = 1 << 1,
        WINDOW_FLAG_NO_ONTOGGLE_ANIMATION = 1 << 2,
        WINDOW_FLAG_NO_MOVE = 1 << 3,
        WINDOW_FLAG_ALWAYS_OPEN = 1 << 4,
    };

    // TextInputFlags defines flags for text input appearance.
    enum TextInputFlags {
        TEXT_INPUT_FLAG_NONE = 0,
        TEXT_INPUT_FLAG_PASSWORD = 1 << 0
    };

    // GroupBoxFlags defines flags for groupbox appearance.
    enum GroupBoxFlags {
        GROUPBOX_FLAG_NONE = 0,
        GROUPBOX_FLAG_LEGACY_DESIGN = 1 << 0,
    };

    // RenderType represents the type of rendering operation.
    enum class RenderType {
        LINE = 1,
        RECT,
        FILLED_RECT,
        TEXT
    };

    /**
     * @struct ControlRender
     * @brief Holds information about a rendering operation for a control.
     */
    struct ControlRender {
        Vec2 draw_position; // Position to start drawing.
        RenderType render_type; // Type of rendering operation.
        Color color; // Color to use for rendering.
        std::string text; // Text to render, if applicable.
        Vec2 size; // Size of the control, if applicable.
        unsigned long font = 0; // Font to use for text, if applicable.
    };

    /**
     * @struct GuiWindowContext
     * @brief Holds the context for a GUI window, including state and rendering information.
     */
    struct GuiWindowContext {
        uint32_t blocking; // Indicates whether input is being blocked.
        std::stack<Vec2> cursor_positions; // Stack of cursor positions.
        std::stack<unsigned long> fonts; // Stack of fonts.
        std::vector<ControlRender> renders; // List of render operations for the window.
        Vec2 position; // Position of the window.
        Vec2 size; // Size of the window.
        Vec2 next_cursor_position; // Next cursor position.
        bool dragging; // Indicates whether the window is being dragged.
        bool opened; // Indicates whether the window is open.
        int alpha; // Alpha transparency of the window.
    };

    /**
     * @struct KeyCodeInfo
     * @brief Represents information about a keyboard key, including its virtual key code and character representations.
     */
    struct KeyCodeInfo {
        int vk; // Virtual key code.
        char regular; // Character when shift is not pressed.
        char shift; // Character when shift is pressed.
    };

    // The globals namespace contains global variables and configurations for the GUI.
    namespace globals {
        // keys_list defines a list of key names for display.
        constexpr std::string_view keys_list[] = {
          "Error", "Left Mouse", "Right Mouse", "Break", "Middle Mouse", "Mouse 4", "Mouse 5",
          "Error", "Backspace", "TAB", "Error", "Error", "Error", "ENTER", "Error", "Error", "SHIFT",
          "CTRL", "ALT", "PAUSE", "CAPS LOCK", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error", "Error", "Error", "Error", "SPACEBAR", "PG UP", "PG DOWN", "END", "HOME", "Left",
          "Up", "Right", "Down", "Error", "Print", "Error", "Print Screen", "Insert", "Delete", "Error", "0", "1",
          "2", "3", "4", "5", "6", "7", "8", "9", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
          "V", "W", "X", "Y", "Z", "Left Windows", "Right Windows", "Error", "Error", "Error", "NUM 0", "NUM 1",
          "NUM 2", "NUM 3", "NUM 4", "NUM 5", "NUM 6", "NUM 7", "NUM 8", "NUM 9", "*", "+", "_", "-", ".", "/", "F1", "F2", "F3",
          "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18", "F19", "F20", "F21",
          "F22", "F23", "F24", "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "NUM LOCK", "SCROLL LOCK", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error", "Error", "Error", "Error", "Error", "Error", "LSHIFT", "RSHIFT", "LCONTROL",
          "RCONTROL", "LMENU", "RMENU", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error", "Error", "Next Track", "Previous Track", "Stop", "Play/Pause", "Error", "Error",
          "Error", "Error", "Error", "Error", ";", "+", ",", "-", ".", "/?", "~", "Error", "Error",
          "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error", "Error", "Error", "Error", "Error", "[{", "\\|", "}]", "'\"", "Error",
          "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error", "Error",
          "Error", "Error"
        };

        // special_characters defines mappings for special characters based on key codes.
        static KeyCodeInfo special_characters[] = {
          {48,  '0',  ')'},
          {49,  '1',  '!'},
          {50,  '2',  '@'},
          {51,  '3',  '#'},
          {52,  '4',  '$'},
          {53,  '5',  '%'},
          {54,  '6',  '^'},
          {55,  '7',  '&'},
          {56,  '8',  '*'},
          {57,  '9',  '('},
          {32,  ' ',  ' '},
          {192, '`',  '~'},
          {189, '-',  '_'},
          {187, '=',  '+'},
          {219, '[',  '{'},
          {220, '\\', '|'},
          {221, ']',  '}'},
          {186, ';',  ':'},
          {222, '\'', '"'},
          {188, ',',  '<'},
          {190, '.',  '>'},
          {191, '/',  '?'}
        };

        // GlobalColors defines colors used throughout the GUI.
        static struct GlobalColors {
            Color window_border_inner_fill{60, 60, 60, 255};
            Color window_border_fill{40, 40, 40, 255};
            Color window_border_color{10, 10, 10, 255};
            Color window_background{40, 40, 40, 255};

            Color control_outline{23, 23, 30, 255};
            Color control_active_or_clicked{108, 92, 231, 255};
            Color control_idle{62, 62, 72, 255};

            Color color_groupbox_bg{50, 50, 50, 255};
            Color color_text{203, 203, 203, 255};
            Color color_text_dimmer{99, 110, 114, 255};
            Color color_slider{108, 92, 231, 255};
            Color color_combo_bg{108, 92, 231, 255};
            Color color_groupbox_header{26, 26, 26, 150};
        } global_colors;

        // GlobalConfig holds global configuration settings for the GUI.
        static struct GlobalConfig {
            Vec2 base_pos{16, 23};
            int item_spacing = 16;
            int menu_toggle_key = VK_INSERT;
        } global_config;

        // Window context for the GUI.
        extern GuiWindowContext window_ctx;

        // Mouse position tracking.
        static Vec2 mouse_pos;
        static Vec2 previous_mouse_pos;

        // Input state tracking.
        static bool key_state[256];
        static bool prev_key_state[256];

        // Indicates if the input loop has started.
        static bool input_loop_started = false;
    }

    /**
     * @brief Polls input for a given window name.
     * @param window_name The name of the window to poll input for.
     */
    GUI_API void poll_input(std::string_view window_name);

    /**
     * @brief Polls input for a given window handle.
     * @param hwnd The handle of the window to poll input for.
     */
    GUI_API void poll_input(HWND hwnd);
    
    /**
    * @namespace input
    * @brief Provides utility functions for input handling, such as key press checks and mouse position checks.
    */
    namespace utils {
      /**
      * @namespace input
      * @brief Provides utility functions for input handling, such as key press checks and mouse position checks.
      */
      namespace input {

          /**
          * @brief Checks if a key was pressed.
          * @param key The key code to check.
          * @return True if the key was pressed, false otherwise.
          */
          bool key_pressed(int key);

          /**
          * @brief Checks if a key is currently down.
          * @param key The key code to check.
          * @return True if the key is down, false otherwise.
          */
          bool key_down(int key);

          /**
          * @brief Checks if a key was released.
          * @param key The key code to check.
          * @return True if the key was released, false otherwise.
          */
          bool key_released(int key);

          /**
          * @brief Checks if the mouse is within a specified region.
          * @param x The x-coordinate of the top-left corner of the region.
          * @param y The y-coordinate of the top-left corner of the region.
          * @param w The width of the region.
          * @param h The height of the region.
          * @return True if the mouse is within the region, false otherwise.
          */
          bool mouse_in_region(int x, int y, int w, int h);
      }

      /**
      * @namespace hash
      * @brief Provides utility functions for hashing and string manipulation.
      */
      namespace hash {

          /**
          * @brief Splits a string into a vector of strings based on a separator character.
          * @param str The string to split.
          * @param separator The character to use as a separator.
          * @return A vector of strings resulting from the split operation.
          */
          std::vector<std::string> split_str(const char* str, char separator);

          /**
          * @brief Computes a hash value for a given string.
          * @param str The string to hash.
          * @param value The initial hash value (default is 0x811c9dc5).
          * @return The computed hash value.
          */
          uint32_t hash(const char* str, uint32_t value = 0x811c9dc5);
      }

      /**
      * @namespace misc
      * @brief Provides miscellaneous utility functions for GUI operations, such as cursor position and font management.
      */
      namespace misc {

          /**
          * @brief Pushes a cursor position onto the stack.
          * @param pos The cursor position to push.
          */
          GUI_API void push_cursor_pos(Vec2 pos);

          /**
          * @brief Pops the top cursor position off the stack.
          * @return The top cursor position.
          */
          GUI_API Vec2 pop_cursor_pos();

          /**
          * @brief Pushes a font onto the stack.
          * @param font The font to push.
          */
          GUI_API void push_font(unsigned long font);

          /**
          * @brief Pops the top font off the stack.
          * @return The top font.
          */
          GUI_API unsigned long pop_font();
      }
    }

    /**
     * @brief Begins a window context for GUI rendering.
     * @param title The title of the window.
     * @param default_size The default size of the window.
     * @param font The font to use for the window's text.
     * @param flags Flags to customize the window's appearance and behavior.
     * @return True if the window should be rendered, false otherwise.
     */
    GUI_API bool begin_window(std::string_view title, Vec2 default_size, unsigned long font, int flags = WINDOW_FLAG_NONE);

    /**
     * @brief Ends the current window context.
     */
    GUI_API void end_window();

    /**
     * @brief Begins a groupbox context within a window.
     * @param title The title of the groupbox.
     * @param size The size of the groupbox.
     * @param flags Flags to customize the groupbox's appearance.
     */
    GUI_API void begin_groupbox(std::string_view title, Vec2 size, int flags = GROUPBOX_FLAG_NONE);

    /**
     * @brief Ends the current groupbox context.
     */
    GUI_API void end_groupbox();

    /**
     * @brief Creates a checkbox control.
     * @param id The identifier for the checkbox.
     * @param value A reference to the variable that the checkbox modifies.
     */
    GUI_API void checkbox(const char* id, bool& value);

    /**
     * @brief Creates a toggle button control.
     * @param id The identifier for the toggle button.
     * @param size The size of the toggle button.
     * @param value A reference to the variable that the toggle button modifies.
     */
    GUI_API void toggle_button(const char* id, Vec2 size, bool& value);

    /**
     * @brief Creates a button control.
     * @param id The identifier for the button.
     * @param size The size of the button.
     * @return True if the button was pressed, false otherwise.
     */
    GUI_API bool button(const char* id, Vec2 size);

    /**
     * @brief Creates a key bind control.
     * @param id The identifier for the key bind.
     * @param value A reference to the variable that stores the key code.
     */
    GUI_API void key_bind(const char* id, int& value);

    /**
     * @brief Creates a text input control.
     * @param id The identifier for the text input.
     * @param value A reference to the variable that stores the input text.
     * @param max_length The maximum length of the input text.
     * @param flags Flags to customize the text input's appearance.
     */
    GUI_API void text_input(const char* id, std::string& value, int max_length = 16, int flags = TEXT_INPUT_FLAG_NONE);

    /**
     * @brief Creates a slider control for integer values.
     * @param id The identifier for the slider.
     * @param min The minimum value of the slider.
     * @param max The maximum value of the slider.
     * @param value A reference to the variable that the slider modifies.
     */
    GUI_API void slider_int(const char* id, int min, int max, int& value);

    /**
     * @brief Creates a slider control for floating-point values.
     * @param id The identifier for the slider.
     * @param min The minimum value of the slider.
     * @param max The maximum value of the slider.
     * @param value A reference to the variable that the slider modifies.
     */
    GUI_API void slider_float(const char* id, float min, float max, float& value);

    /**
     * @brief Creates a combobox control.
     * @param id The identifier for the combobox.
     * @param items A list of items to display in the combobox.
     * @param value A reference to the variable that stores the index of the selected item.
     */
    GUI_API void combobox(const char* id, std::vector<std::string> items, int& value);

    /**
     * @brief Creates a multi-selection combobox control.
     * @param id The identifier for the multi-selection combobox.
     * @param items A list of items to display in the combobox, with selection states.
     */
    GUI_API void multi_combobox(const char* id, std::vector<MultiSelectableItem> items);

    /**
     * @brief Creates a listbox control.
     * @param id The identifier for the listbox.
     * @param items A list of items to display in the listbox, with selection states.
     */
    GUI_API void listbox(const char* id, std::vector<MultiSelectableItem> items);

    /**
     * @brief Creates a control that displays clickable text.
     * @param id The identifier for the clickable text.
     * @return True if the text was clicked, false otherwise.
     */
    GUI_API bool clickable_text(const char* id);

    /**
     * @brief Displays text.
     * @param text The text to display.
     */
    GUI_API void text(const char* text);

    /**
     * @brief Creates a dummy control for spacing.
     */
    GUI_API void dummy();

    /**
     * @brief Advances to the next column in a multi-column layout.
     * @param pusher_x The x offset to the next column.
     * @param pusher_y The y offset to the next column.
     */
    GUI_API void next_column(int pusher_x = 174, int pusher_y = 14);

    /**
     * @brief Positions the next control on the same line as the previous control.
     * @param x_axis The x offset from the start of the line. A value of -1 maintains the current x position.
     */
    GUI_API void same_line(float x_axis = -1);

    /**
     * @brief Backs up to the previous line in the layout.
     */
    GUI_API void backup_line();
}

#endif // _MINIMAL_GUI_HPP