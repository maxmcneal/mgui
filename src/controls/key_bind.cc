//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a key bind control that allows users to assign keyboard keys to actions.
 * @param id A unique identifier for the key bind control, potentially including an ID and label separated by '#'.
 * @param value A reference to an integer representing the key code currently bound.
 */
void mgui::key_bind(const char* id, int& value) {
    auto id_split = utils::hash::split_str(id, '#');

    const int control_width = 80;
    const int control_height = 20;

    // Ensure the value is within the valid range for key codes.
    value = std::clamp(value, 0, 255);

    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 draw_pos{globals::window_ctx.position.x + cursor_pos.x + 14, globals::window_ctx.position.y + cursor_pos.y};

    const bool inlined = id_split[0].empty();

    int text_width, text_height;
    // Render the label if provided.
    if (!inlined) {
        functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

        globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y - 4}, 
            RenderType::TEXT, globals::global_colors.color_text, id_split[0], Vec2{0, 0}, font});

        draw_pos.y += text_height;
    }

    const bool active = globals::window_ctx.blocking == utils::hash::hash(id);

    // Check for user interaction to activate the key binding mode.
    if (const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, control_width, control_height);
        hovered && utils::input::key_pressed(VK_LBUTTON) && globals::window_ctx.blocking == 0) {
        globals::window_ctx.blocking = utils::hash::hash(id);
    } else if (active) {
        for (int i = 0; i < 256; i++) {
            if (utils::input::key_pressed(i)) {
                if (globals::keys_list[i] != "Error") {
                    value = i;
                }

                globals::window_ctx.blocking = 0;
                break;
            }
        }
    }

    // Render the key bind control.
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 4, draw_pos.y + 4}, 
        RenderType::TEXT, globals::global_colors.color_text, active ? "Press any key" : globals::keys_list[value].data(), Vec2{0, 0}, font});
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, active ? globals::global_colors.control_active_or_clicked : globals::global_colors.control_idle, "", Vec2{control_width, control_height}, font});
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, globals::global_colors.control_outline, "", Vec2{control_width + 2, control_height + 2}, font});

    // Adjust cursor position for subsequent controls.
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + control_width + globals::global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + control_height + globals::global_config.item_spacing + (inlined ? 0 : text_height + 4)});

    // Restore the font for subsequent controls.
    utils::misc::push_font(font);
}