//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a combobox control allowing item selection from a dropdown list.
 * @param id A unique identifier for the combobox, which may include an ID and label separated by '#'.
 * @param items A list of items to be displayed in the combobox.
 * @param value A reference to the index of the currently selected item.
 */
void mgui::combobox(const char* id, std::vector<std::string> items, int& value) {
    // Split the ID to separate the label from the unique identifier.
    auto id_split = utils::hash::split_str(id, '#');

    const int control_width = 70;
    const int control_height = 20;

    // Clamp the value to ensure it's within the range of items.
    value = std::clamp(value, 0, static_cast<int>(items.size()) - 1);

    // Retrieve the last used font and cursor position.
    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 draw_pos{globals::window_ctx.position.x + cursor_pos.x + 14, globals::window_ctx.position.y + cursor_pos.y};

    // Check if the combobox label should be inlined.
    const bool inlined = id_split[0].empty();

    // Render the label if not inlined.
    if (!inlined) {
        int text_width, text_height;
        functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

        globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y - 4}, 
            RenderType::TEXT, globals::global_colors.color_text, id_split[0], Vec2{0, 0}, font});

        draw_pos.y += text_height;
    }

    // Render the combobox control, including the dropdown indicator ('+') and selected item.
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + control_width - 10, draw_pos.y + 4}, 
        RenderType::TEXT, globals::global_colors.color_text, "+", Vec2{0, 0}, font});

    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 4, draw_pos.y + 4}, 
        RenderType::TEXT, globals::global_colors.color_text, items.at(value), Vec2{0, 0}, font});

    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, globals::global_colors.control_idle, "", Vec2{control_width - 2, control_height - 2}, font});

    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, globals::global_colors.control_outline, "", Vec2{control_width, control_height}, font});

    // Handle dropdown logic and item selection.
    if (const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, control_width, control_height); 
        hovered && utils::input::key_pressed(VK_LBUTTON) && globals::window_ctx.blocking == 0) {
        globals::window_ctx.blocking = utils::hash::hash(id); // Block further interactions.
    } else if (globals::window_ctx.blocking == utils::hash::hash(id)) {
        for (size_t i = 0; i < items.size(); ++i) {
            bool item_hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y + control_height * (i + 1), control_width, control_height);

            // Render each dropdown item.
            bool selected = value == static_cast<int>(i);
            globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 4, draw_pos.y + control_height * (i + 1) + 4}, 
                RenderType::TEXT, selected ? globals::global_colors.control_active_or_clicked : globals::global_colors.color_text, items[i], Vec2{0, 0}, font});

            globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + control_height * (i + 1) + 1}, 
                RenderType::FILLED_RECT, item_hovered ? globals::global_colors.color_combo_bg : globals::global_colors.control_idle, "", Vec2{control_width - 2, control_height - 2}, font});

            globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y + control_height * (i + 1)}, 
                RenderType::FILLED_RECT, globals::global_colors.control_outline, "", Vec2{control_width, control_height}, font});

            // Update the selected value if an item is clicked.
            if (item_hovered && utils::input::key_pressed(VK_LBUTTON)) {
                globals::window_ctx.blocking = 0;
                value = static_cast<int>(i);
                break; // Exit the loop after selection to prevent multiple selections.
            }
        }
    }

    // Adjust cursor position for subsequent controls, considering item spacing and label adjustments.
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + control_width + globals::global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + control_height + globals::global_config.item_spacing + (inlined ? 0 : 12)});

    // Restore the font for subsequent controls.
    utils::misc::push_font(font);
}

/**
 * @brief Creates a multi-selection combobox control allowing multiple item selections.
 * @param id A unique identifier for the combobox, which may include an ID and label separated by '#'.
 * @param items A vector of multi_select_item structures representing the items and their selection states.
 */
void mgui::multi_combobox(const char* id, std::vector<MultiSelectableItem> items) {
    auto id_split = utils::hash::split_str(id, '#');

    const int control_width = 100;
    const int control_height = 20;

    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 draw_pos{globals::window_ctx.position.x + cursor_pos.x + 14, globals::window_ctx.position.y + cursor_pos.y};

    const bool inlined = id_split[0].empty();

    // Render the label if not inlined.
    if (!inlined) {
        int text_width, text_height;
        functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

        globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y - 4}, 
            RenderType::TEXT, globals::global_colors.color_text, id_split[0], Vec2{0, 0}, font});

        draw_pos.y += text_height;
    }

    // Compose the value string from selected items.
    std::string value_str;
    for (const auto& item : items) {
        if (*item.value) {
            if (!value_str.empty()) value_str += ", ";
            value_str += item.name;
        }
    }

    // Handle overflow in value string length.
    int text_width, text_height;
    functions.get_text_size(font, value_str.c_str(), text_width, text_height);
    if (text_width > control_width - 18) {
        value_str.resize(control_width / 10);
        value_str += "...";
    }
    if (value_str.empty()) value_str = "None";

    // Render the combobox with the current selections.
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + control_width - 10, draw_pos.y + 4}, 
        RenderType::TEXT, globals::global_colors.color_text, "+", Vec2{0, 0}, font});
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 4, draw_pos.y + 4}, 
        RenderType::TEXT, globals::global_colors.color_text, value_str, Vec2{0, 0}, font});
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, globals::global_colors.control_idle, "", Vec2{control_width - 2, control_height - 2}, font});
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, globals::global_colors.control_outline, "", Vec2{control_width, control_height}, font});

    // Handling interactions and rendering dropdown list
    const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, control_width, control_height);
    if (hovered && utils::input::key_pressed(VK_LBUTTON) && globals::window_ctx.blocking == 0) {
        globals::window_ctx.blocking = utils::hash::hash(id); // Open or close the dropdown
    }

    // Check if the dropdown is active
    if (globals::window_ctx.blocking == utils::hash::hash(id)) {
        // Calculate dropdown position and size
        float dropdown_y = draw_pos.y + control_height + 5; // Slightly below the combobox
        float dropdown_height = control_height * std::min<int>(static_cast<int>(items.size()), 5); // Show up to 5 items, then scroll

        // Render dropdown background
        globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, dropdown_y}, 
            RenderType::FILLED_RECT, globals::global_colors.control_idle, "", Vec2{control_width, dropdown_height}, font});

        // Iterate through items for rendering and selection logic
        for (size_t i = 0; i < items.size(); ++i) {
            bool item_hovered = utils::input::mouse_in_region(draw_pos.x, dropdown_y + control_height * i, control_width, control_height);
            bool selected = *items[i].value;

            // Render each item
            globals::window_ctx.renders.emplace_back(ControlRender{ Vec2{draw_pos.x + 4, dropdown_y + control_height * i + 4},
                RenderType::TEXT, selected ? globals::global_colors.control_active_or_clicked : globals::global_colors.color_text, std::string(items[i].name), Vec2{0, 0}, font });

            // Handle item selection
            if (item_hovered && utils::input::key_pressed(VK_LBUTTON)) {
                *items[i].value = !*items[i].value; // Toggle selection
                globals::window_ctx.blocking = 0; // Optionally close the dropdown after selection
            }
        }

        // Close the dropdown if clicking outside
        if (!hovered && utils::input::key_pressed(VK_LBUTTON)) {
            globals::window_ctx.blocking = 0;
        }
    }

    // Adjust cursor position for subsequent controls based on whether the dropdown is displayed
    int additional_height = (globals::window_ctx.blocking == utils::hash::hash(id)) ? (control_height * std::min<int>(static_cast<int>(items.size()), 5) + 5) : 0;
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + control_height + globals::global_config.item_spacing + additional_height + (inlined ? 0 : 12)});

    // Restore the font for subsequent controls.
    utils::misc::push_font(font);
}