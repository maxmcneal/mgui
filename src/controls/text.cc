//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a clickable text control.
 * @param id The identifier for the clickable text, which can include a label separated by '#'.
 * @return True if the text was clicked in this frame, false otherwise.
 */
bool mgui::clickable_text(const char* id) {
    std::vector<std::string> id_split = utils::hash::split_str(id, '#');

    const unsigned long font = utils::misc::pop_font();

    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    const Vec2 draw_pos{globals::window_ctx.position.x + cursor_pos.x, globals::window_ctx.position.y + cursor_pos.y};

    int text_width, text_height;
    functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

    const bool active = globals::window_ctx.blocking == utils::hash::hash(id);
    const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, text_width, text_height);

    globals::window_ctx.renders.emplace_back(ControlRender{draw_pos, RenderType::TEXT, (hovered || globals::window_ctx.blocking == utils::hash::hash(id)) ? globals::global_colors.control_active_or_clicked : globals::global_colors.color_text, id_split[0], Vec2{0, 0}, font});

    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + text_width + globals::global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + text_height / 2 + globals::global_config.item_spacing});

    bool result = false;

    if (!active && hovered && utils::input::key_pressed(VK_LBUTTON)) {
        globals::window_ctx.blocking = utils::hash::hash(id);
    } else if (active && !utils::input::key_down(VK_LBUTTON)) {
        globals::window_ctx.blocking = 0;
        result = hovered;
    }

    utils::misc::push_font(font);

    return result;
}

/**
 * @brief Displays a text label at the current cursor position.
 * @param text The text to be displayed.
 */
void mgui::text(const char* text) {
    const unsigned long font = utils::misc::pop_font();

    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    const Vec2 draw_pos{globals::window_ctx.position.x + cursor_pos.x, globals::window_ctx.position.y + cursor_pos.y};

    int text_width, text_height;
    functions.get_text_size(font, text, text_width, text_height);

    globals::window_ctx.renders.emplace_back(ControlRender{draw_pos, RenderType::TEXT, globals::global_colors.color_text, text, Vec2{0, 0}, font});

    // Adjust cursor position for subsequent controls.
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + text_width + globals::global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + text_height / 2 + globals::global_config.item_spacing});

    utils::misc::push_font(font);
}