//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Begins a group box with an optional title and specific size.
 * @param title The title of the group box.
 * @param size The size of the group box.
 * @param flags Flags to customize the group box appearance.
 */
void mgui::begin_groupbox(std::string_view title, const Vec2 size, const int flags) {
    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 draw_pos{window_ctx.position.x + cursor_pos.x, window_ctx.position.y + cursor_pos.y};

    int text_width = 0, text_height = 0;

    // Calculate text size if title is not empty
    if (!title.empty()) {
        functions.get_text_size(font, title.data(), text_width, text_height);
    }

    const int header_height = title.empty() ? 16 : text_height + 3;

    // Drawing group box based on legacy design flag
    if (flags & GROUPBOX_FLAG_LEGACY_DESIGN) {
        functions.draw_rect(draw_pos.x - 1, draw_pos.y - 1, size.x + 2, size.y + 2, global_colors.control_outline);
        functions.draw_filled_rect(draw_pos.x, draw_pos.y, size.x, size.y, global_colors.color_groupbox_bg);

        // Draw title for legacy design
        if (!title.empty()) {
            functions.draw_text(draw_pos.x + 4, draw_pos.y - 8, global_colors.color_text, font, false, title.data());
        }
    } else {
        // Modern design with header
        functions.draw_rect(draw_pos.x - 1, draw_pos.y - 1, size.x + 2, size.y + 2, global_colors.control_outline);
        functions.draw_filled_rect(draw_pos.x, draw_pos.y, size.x, size.y, global_colors.color_groupbox_bg);
        functions.draw_filled_rect(draw_pos.x, draw_pos.y, size.x, header_height, global_colors.color_groupbox_header);
        functions.draw_line(draw_pos.x, draw_pos.y + header_height, draw_pos.x + size.x, draw_pos.y + header_height, global_colors.control_outline);

        // Centered title for modern design
        if (!title.empty()) {
            functions.draw_text(draw_pos.x + size.x / 2 - text_width / 2, draw_pos.y + 2, global_colors.color_text, font, false, title.data());
        }
    }

    // Update next cursor position based on the group box size
    window_ctx.next_cursor_position = Vec2{cursor_pos.x, cursor_pos.y + size.y + 10};

    // Adjust cursor position inside the group box
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + 8, cursor_pos.y + ((flags & GROUPBOX_FLAG_LEGACY_DESIGN) ? 0 : header_height) + 8});

    // Restore the font for subsequent controls
    utils::misc::push_font(font);
}

/**
 * @brief Ends a group box and resets the cursor position for subsequent controls.
 */
void mgui::end_groupbox() {
    // Reset the cursor position to the one stored at the beginning of the groupbox
    utils::misc::push_cursor_pos(window_ctx.next_cursor_position);

    // Clear the next cursor position to indicate the end of the groupbox
    window_ctx.next_cursor_position = Vec2{};
}