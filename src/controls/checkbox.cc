//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a checkbox control.
 * @param id A unique identifier for the checkbox, which may include an ID and label separated by '#'.
 * @param value A reference to a boolean value tied to the checkbox state.
 */
void mgui::checkbox(const char* id, bool& value) {
    // Split the ID to separate the label from the unique identifier.
    auto id_split = utils::hash::split_str(id, '#');

    const int control_height = 8;
    const int control_width = 8;

    // Retrieve the last used font and cursor position.
    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    const Vec2 draw_pos{window_ctx.position.x + cursor_pos.x, window_ctx.position.y + cursor_pos.y};

    // Calculate text size for layout purposes.
    int text_width, text_height;
    functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

    // Determine if the checkbox is currently active based on the hashed ID.
    const bool active = window_ctx.blocking == utils::hash::hash(id);

    // Toggle the value if the checkbox is active and the left mouse button is released.
    if (const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, control_width + 6 + text_width, control_height); 
        !active && hovered && utils::input::key_pressed(VK_LBUTTON)) {
        window_ctx.blocking = utils::hash::hash(id);
    } else if (active && !utils::input::key_down(VK_LBUTTON)) {
        window_ctx.blocking = 0;
        value = !value;
    }

    // Render the checkbox label.
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 14, draw_pos.y - 2}, 
        RenderType::TEXT, value ? global_colors.color_text : global_colors.color_text_dimmer, id_split[0], Vec2{0, 0}, font});
    
    // Render the checkbox itself, indicating its state.
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, value ? global_colors.control_active_or_clicked : global_colors.control_idle, "", 
        Vec2{control_width - 2, control_height - 2}, font});
    
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, global_colors.control_outline, "", Vec2{control_width, control_height}, font});

    // Adjust cursor position for subsequent controls, accounting for text width and spacing.
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + 14 + text_width + global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + control_height + global_config.item_spacing});

    // Restore the font for subsequent controls.
    utils::misc::push_font(font);
}