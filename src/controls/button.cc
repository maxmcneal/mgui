//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a button control.
 * @param id A unique identifier for the button, which may include an ID and label separated by '#'.
 * @param size The size of the button.
 * @return True if the button is clicked during this frame, false otherwise.
 */
bool mgui::button(const char* id, const Vec2 size) {
    // Split the ID to separate the label from the unique identifier.
    auto id_split = utils::hash::split_str(id, '#');

    // Retrieve the last used font and cursor position.
    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    const Vec2 draw_pos{window_ctx.position.x + cursor_pos.x, window_ctx.position.y + cursor_pos.y};

    // Determine if the button is currently active based on the hashed ID.
    const bool active = window_ctx.blocking == utils::hash::hash(id);

    bool result = false;
    // Check if the button is hovered over and clicked.
    if (const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, size.x, size.y); 
        !active && hovered && utils::input::key_pressed(VK_LBUTTON)) {
        window_ctx.blocking = utils::hash::hash(id); // Block further interactions while active.
    } else if (active && !utils::input::key_down(VK_LBUTTON)) {
        window_ctx.blocking = 0; // Release block when mouse button released.
        result = hovered; // Only return true if the button was actually hovered over.
    }

    // Calculate text size for proper centering.
    int text_width, text_height;
    functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

    // Render the button label centered.
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + size.x / 2 - text_width / 2, draw_pos.y + size.y / 2 - text_height / 2}, 
        RenderType::TEXT, global_colors.color_text, id_split[0], Vec2{0, 0}, font});
    
    // Render the button background.
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, active ? global_colors.control_active_or_clicked : global_colors.control_idle, "", 
        Vec2{size.x - 2, size.y - 2}, font});
    
    // Render the button outline.
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, global_colors.control_outline, "", size, font});

    // Adjust cursor position for subsequent controls.
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + size.x + global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + size.y + global_config.item_spacing});

    // Restore the font for subsequent controls.
    utils::misc::push_font(font);

    return result;
}