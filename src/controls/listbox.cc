//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a listbox control allowing for multiple selections from a list of items.
 * @param id A unique identifier for the listbox, potentially including an ID and label separated by '#'.
 * @param items A vector of MultiSelectableItem structures representing the items and their selection states.
 */
void mgui::listbox(const char* id, std::vector<MultiSelectableItem> items) {
    auto id_split = utils::hash::split_str(id, '#');

    const int control_width = 100;
    const int control_height = 20; // Height for individual items

    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 draw_pos{globals::window_ctx.position.x + cursor_pos.x, globals::window_ctx.position.y + cursor_pos.y};

    // Render the label if provided.
    if (!id_split[0].empty()) {
        int text_width, text_height;
        functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

        globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y - 4}, 
            RenderType::TEXT, globals::global_colors.color_text, id_split[0], Vec2{0, 0}, font});

        draw_pos.y += text_height;
    }

    // Iterate over items to render each one.
    for (size_t i = 0; i < items.size(); ++i) {
        const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y + control_height * i, control_width, control_height);

        // Toggle item selection on click.
        if (hovered && utils::input::key_pressed(VK_LBUTTON) && globals::window_ctx.blocking == 0) {
            *items[i].value = !*items[i].value;
            globals::window_ctx.blocking = utils::hash::hash(id); // Optional: Reset blocking if needed.
        }

        // Render the item text, highlighting if selected or hovered.
        globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 4, draw_pos.y + control_height * i + 4}, 
            RenderType::TEXT, *items[i].value ? globals::global_colors.control_active_or_clicked : hovered ? globals::global_colors.color_slider : globals::global_colors.color_text, 
            std::string(items[i].name), Vec2{0, 0}, font});
    }

    // Render the listbox background and outline.
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, globals::global_colors.control_idle, "", Vec2{control_width - 2, static_cast<float>(control_height * items.size() - 2)}, font});
    globals::window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, globals::global_colors.control_outline, "", Vec2{control_width, static_cast<float>(control_height * items.size())}, font});

    // Adjust cursor position for subsequent controls, accounting for the entire listbox height.
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, draw_pos.y + control_height * items.size() + (id_split[0].empty() ? 0 : 12)});

    // Restore the font for subsequent controls.
    utils::misc::push_font(font);
}
