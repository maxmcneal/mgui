//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Begins a window context for GUI rendering, handling window opening logic and animations.
 * @param title The title of the window, displayed in the title bar.
 * @param default_size The default size of the window if not explicitly set.
 * @param font The font used for rendering text within the window.
 * @param flags Flags to customize the window's appearance and behavior.
 * @return True if the window is open or in the process of opening/closing, false otherwise.
 * @exception Throws std::exception if the input loop has not been started.
 */
bool mgui::begin_window(std::string_view title, const Vec2 default_size, const unsigned long font, const int flags) {
    if (!input_loop_started)
        throw std::exception("Input loop didn't start or didn't start properly.");

    if (!(flags & WINDOW_FLAG_ALWAYS_OPEN)) {
        if (utils::input::key_pressed(global_config.menu_toggle_key))
            window_ctx.opened = !window_ctx.opened;
    } else {
        window_ctx.opened = true;
    }

    if (const int prev_alpha = window_ctx.alpha; !(flags & WINDOW_FLAG_NO_ONTOGGLE_ANIMATION)) {
        const int fade_factor = static_cast<int>(1.0f / 0.15f * functions.get_frametime() * 255);
        window_ctx.alpha = std::clamp(window_ctx.alpha + (window_ctx.opened ? fade_factor : -fade_factor), 0, 255);

        // Adjust the alpha value of global colors based on window animation.
        auto adjust_alpha = [&](Color& color) {
            color.a = window_ctx.alpha;
        };

        adjust_alpha(global_colors.window_border_inner_fill);
        adjust_alpha(global_colors.window_border_fill);
        adjust_alpha(global_colors.window_border_color);
        adjust_alpha(global_colors.window_background);
        adjust_alpha(global_colors.control_outline);
        adjust_alpha(global_colors.control_active_or_clicked);
        adjust_alpha(global_colors.control_idle);
        adjust_alpha(global_colors.color_groupbox_bg);
        adjust_alpha(global_colors.color_text);
        adjust_alpha(global_colors.color_text_dimmer);
        adjust_alpha(global_colors.color_slider);
    }

    if (window_ctx.opened || window_ctx.alpha > 0) {
        if (!(flags & WINDOW_FLAG_NO_MOVE)) {
            if ((flags & WINDOW_FLAG_NO_BORDER ? utils::input::mouse_in_region(window_ctx.position.x + 9, window_ctx.position.y + 14, window_ctx.size.x - 18, 14)
                : utils::input::mouse_in_region(window_ctx.position.x - 6, window_ctx.position.y - 10, window_ctx.size.x + 12, 16))
                && utils::input::key_pressed(VK_LBUTTON) && !window_ctx.dragging) {
                window_ctx.dragging = true;
            }
            else if (utils::input::key_down(VK_LBUTTON) && window_ctx.dragging) {
                const Vec2 mouse_delta{ mouse_pos.x - previous_mouse_pos.x, mouse_pos.y - previous_mouse_pos.y };
                const Vec2 new_position{ window_ctx.position.x + mouse_delta.x, window_ctx.position.y + mouse_delta.y };

                window_ctx.position = new_position;
            }
            else if (!utils::input::key_down(VK_LBUTTON) && window_ctx.dragging) {
                window_ctx.dragging = false;
            }
        }

        // Update window size to default if not set.
        if (window_ctx.size.x < 1 && window_ctx.size.y < 1) {
            window_ctx.size = default_size;
        }

        // Render window border and background based on flags.
        if (!(flags & WINDOW_FLAG_NO_BORDER)) {
            functions.draw_filled_rect(window_ctx.position.x - 6, window_ctx.position.y - 10, window_ctx.size.x + 12, window_ctx.size.y + 16, global_colors.window_border_inner_fill);
            functions.draw_filled_rect(window_ctx.position.x - 5, window_ctx.position.y - 9, window_ctx.size.x + 10, window_ctx.size.y + 14, global_colors.window_border_color);
            functions.draw_filled_rect(window_ctx.position.x - 4, window_ctx.position.y - 8, window_ctx.size.x + 8, window_ctx.size.y + 12, global_colors.window_border_fill);
            functions.draw_filled_rect(window_ctx.position.x, window_ctx.position.y + 7, window_ctx.size.x, window_ctx.size.y - 7, global_colors.window_border_color);
            functions.draw_filled_rect(window_ctx.position.x + 1, window_ctx.position.y + 8, window_ctx.size.x - 2, window_ctx.size.y - 9, global_colors.window_border_inner_fill);
            functions.draw_filled_rect(window_ctx.position.x + 8, window_ctx.position.y + 15, window_ctx.size.x - 16, window_ctx.size.y - 23, global_colors.window_border_color);
        }

        // Render title bar if enabled.
        if (!(flags & WINDOW_FLAG_NO_TITLEBAR)) {
            functions.draw_text(window_ctx.position.x + window_ctx.size.x * 0.5f, window_ctx.position.y + 8, global_colors.color_text, font, true, title.data());
        }

        // Render window background.
        functions.draw_filled_rect(window_ctx.position.x + 9, window_ctx.position.y + 16, window_ctx.size.x - 18, window_ctx.size.y - 25, global_colors.window_background);

        utils::misc::push_font(font);
        utils::misc::push_cursor_pos(global_config.base_pos);
    }

    return window_ctx.opened || window_ctx.alpha > 0;
}

/**
 * @brief Ends the current window context, rendering all queued drawing operations.
 */
void mgui::end_window() {
    // Iterate through the render queue in reverse order to render the controls.
    for (int i = globals::window_ctx.renders.size() - 1; i >= 0; --i) {
        const auto& render_item = globals::window_ctx.renders[i];
        switch (render_item.render_type) {
            case RenderType::LINE:
                functions.draw_line(render_item.draw_position.x, render_item.draw_position.y, render_item.size.x, render_item.size.y, render_item.color);
                break;
            case RenderType::RECT:
                functions.draw_rect(render_item.draw_position.x, render_item.draw_position.y, render_item.size.x, render_item.size.y, render_item.color);
                break;
            case RenderType::FILLED_RECT:
                functions.draw_filled_rect(render_item.draw_position.x, render_item.draw_position.y, render_item.size.x, render_item.size.y, render_item.color);
                break;
            case RenderType::TEXT:
                functions.draw_text(render_item.draw_position.x, render_item.draw_position.y, render_item.color, render_item.font, false, render_item.text.c_str());
                break;
        }
    }

    // Clear the render queue now that all items have been processed.
    globals::window_ctx.renders.clear();

    // Clear any remaining cursor positions.
    while (!globals::window_ctx.cursor_positions.empty())
        globals::window_ctx.cursor_positions.pop();
}
