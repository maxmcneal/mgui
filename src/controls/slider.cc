//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates an integer slider control.
 * @param id A unique identifier for the slider, potentially including an ID and label separated by '#'.
 * @param min The minimum value of the slider.
 * @param max The maximum value of the slider.
 * @param value A reference to the variable that holds the current value of the slider.
 */
void mgui::slider_int(const char* id, const int min, const int max, int& value) {
    auto id_split = utils::hash::split_str(id, '#');

    const int control_width = 120;
    const int control_height = 10; // Height for the slider track

    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 draw_pos{window_ctx.position.x + cursor_pos.x, window_ctx.position.y + cursor_pos.y};

    int text_width, text_height;
    // Render label if provided
    if (!id_split[0].empty()) {
        functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

        window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y - 4}, 
            RenderType::TEXT, global_colors.color_text, id_split[0], Vec2{0, 0}, font});

        draw_pos.y += text_height;
    }

    // Adjust the value based on user interaction
    value = std::clamp(value, min, max); // Ensure value is within bounds

    // Check for slider manipulation
    if (window_ctx.blocking == 0) {
        if (utils::input::mouse_in_region(draw_pos.x - (control_height - 2), draw_pos.y, 8, 10) && utils::input::key_pressed(VK_LBUTTON))
            value = std::clamp(value - 1, min, max);
        else if (utils::input::mouse_in_region(draw_pos.x + control_width, draw_pos.y, 8, 10) && utils::input::key_pressed(VK_LBUTTON))
            value = std::clamp(value + 1, min, max);
    }

    // Handle dragging to set the value
    if (const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, control_width, control_height); 
        hovered && utils::input::key_pressed(VK_LBUTTON) && window_ctx.blocking == 0) {
        window_ctx.blocking = utils::hash::hash(id);
    }
    else if (utils::input::key_down(VK_LBUTTON) && window_ctx.blocking == utils::hash::hash(id)) {
        float value_unmapped = std::clamp(static_cast<float>(mouse_pos.x - draw_pos.x), 0.0f, static_cast<float>(control_width));
        value = static_cast<int>(value_unmapped / control_width * (max - min) + min);
    }
    else if (!utils::input::key_down(VK_LBUTTON) && window_ctx.blocking == utils::hash::hash(id)) {
        window_ctx.blocking = 0;
    }

    // Visual representation of the slider value
    const float dynamic_width = static_cast<float>(value - min) / (max - min) * control_width;

    // Render the slider
    std::string value_str = std::to_string(value);
    functions.get_text_size(font, value_str.c_str(), text_width, text_height);

    float text_x = draw_pos.x + dynamic_width - text_width / 2; // Center text on the handle
    text_x = std::clamp(text_x, draw_pos.x, draw_pos.x + control_width - text_width); // Clamp text position within slider bounds

    // Render the slider components
    window_ctx.renders.emplace_back(ControlRender{Vec2{text_x, draw_pos.y + (control_height - text_height) / 2}, 
        RenderType::TEXT, global_colors.color_text, value_str, Vec2{0, 0}, font});
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, global_colors.color_slider, "", Vec2{dynamic_width, control_height - 2}, font});
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, global_colors.control_outline, "", Vec2{control_width, control_height}, font});

    // Adjust cursor position for subsequent controls
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + control_width + global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, draw_pos.y + control_height + global_config.item_spacing + (id_split[0].empty() ? 0 : text_height + 4)});

    // Restore the font for subsequent controls
    utils::misc::push_font(font);
}

/**
 * @brief Creates a floating-point slider control.
 * @param id A unique identifier for the slider, potentially including an ID and label separated by '#'.
 * @param min The minimum value of the slider.
 * @param max The maximum value of the slider.
 * @param value A reference to the variable that holds the current value of the slider.
 */
void mgui::slider_float(const char* id, const float min, const float max, float& value) {
    auto id_split = utils::hash::split_str(id, '#');

    const int control_width = 120;
    const int control_height = 10; // Height for the slider track

    const unsigned long font = utils::misc::pop_font();
    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    Vec2 draw_pos{window_ctx.position.x + cursor_pos.x + 14, window_ctx.position.y + cursor_pos.y};

    int text_width, text_height;
    // Render label if provided
    if (!id_split[0].empty()) {
        functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

        window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y - 4}, 
            RenderType::TEXT, global_colors.color_text, id_split[0], Vec2{0, 0}, font});

        draw_pos.y += text_height;
    }

    // Adjust the value based on user interaction
    value = std::clamp(value, min, max); // Ensure value is within bounds

    // Check for slider manipulation
    if (window_ctx.blocking == 0) {
        if (utils::input::mouse_in_region(draw_pos.x - (control_height - 2), draw_pos.y, 8, 10) && utils::input::key_pressed(VK_LBUTTON))
            value = std::clamp(value - 1.0f, min, max);
        else if (utils::input::mouse_in_region(draw_pos.x + control_width, draw_pos.y, 8, 10) && utils::input::key_pressed(VK_LBUTTON))
            value = std::clamp(value + 1.0f, min, max);
    }

    // Handle dragging to set the value
    if (const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, control_width, control_height); 
        hovered && utils::input::key_pressed(VK_LBUTTON) && window_ctx.blocking == 0) {
        window_ctx.blocking = utils::hash::hash(id);
    }
    else if (utils::input::key_down(VK_LBUTTON) && window_ctx.blocking == utils::hash::hash(id)) {
        float value_unmapped = std::clamp(static_cast<float>(mouse_pos.x - draw_pos.x), 0.0f, static_cast<float>(control_width));
        value = (value_unmapped / static_cast<float>(control_width)) * (max - min) + min;
    }
    else if (!utils::input::key_down(VK_LBUTTON) && window_ctx.blocking == utils::hash::hash(id)) {
        window_ctx.blocking = 0;
    }

    // Visual representation of the slider value
    const float dynamic_width = (value - min) / (max - min) * control_width;

    // Render the slider
    std::string value_str = std::to_string(value);
    functions.get_text_size(font, value_str.c_str(), text_width, text_height);

    float text_x = draw_pos.x + dynamic_width - text_width / 2; // Center text on the handle
    text_x = std::clamp(text_x, draw_pos.x, draw_pos.x + control_width - text_width); // Clamp text position within slider bounds

    // Render the slider components
    window_ctx.renders.emplace_back(ControlRender{Vec2{text_x, draw_pos.y + (control_height - text_height) / 2}, 
        RenderType::TEXT, global_colors.color_text, value_str, Vec2{0, 0}, font});
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, 
        RenderType::FILLED_RECT, global_colors.color_slider, "", Vec2{dynamic_width, control_height - 2}, font});
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x, draw_pos.y}, 
        RenderType::FILLED_RECT, global_colors.control_outline, "", Vec2{control_width, control_height}, font});

    // Adjust cursor position for subsequent controls
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + control_width + global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, draw_pos.y + control_height + global_config.item_spacing + (id_split[0].empty() ? 0 : text_height + 4)});

    // Restore the font for subsequent controls
    utils::misc::push_font(font);
}
