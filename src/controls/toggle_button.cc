//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Creates a toggle button control.
 * @param id The identifier for the toggle button, possibly including a label separated by '#'.
 * @param size The size of the toggle button.
 * @param value A reference to the boolean variable that the button toggles.
 */
void mgui::toggle_button(const char* id, const Vec2 size, bool& value) {
    std::vector<std::string> id_split = utils::hash::split_str(id, '#');

    const unsigned long font = utils::misc::pop_font();

    const Vec2 cursor_pos = utils::misc::pop_cursor_pos();
    const Vec2 draw_pos{window_ctx.position.x + cursor_pos.x, window_ctx.position.y + cursor_pos.y};

    const bool active = window_ctx.blocking == utils::hash::hash(id);

    // Check if the toggle button is hovered and clicked to toggle its value.
    if (const bool hovered = utils::input::mouse_in_region(draw_pos.x, draw_pos.y, size.x, size.y); !active && hovered && utils::input::key_pressed(VK_LBUTTON)) {
        window_ctx.blocking = utils::hash::hash(id);
    } else if (active && !utils::input::key_down(VK_LBUTTON)) {
        window_ctx.blocking = 0;
        value = !value;
    }

    // Calculate text dimensions for centering the label within the button.
    int text_width, text_height;
    functions.get_text_size(font, id_split[0].c_str(), text_width, text_height);

    // Render the button's label, background, and outline.
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + size.x / 2 - text_width / 2, draw_pos.y + size.y / 2 - text_height / 2}, RenderType::TEXT, global_colors.color_text, id_split[0], Vec2{0, 0}, font});
    window_ctx.renders.emplace_back(ControlRender{Vec2{draw_pos.x + 1, draw_pos.y + 1}, RenderType::FILLED_RECT, value ? global_colors.control_active_or_clicked : global_colors.control_idle, "", Vec2{size.x, size.y}});
    window_ctx.renders.emplace_back(ControlRender{draw_pos, RenderType::FILLED_RECT, global_colors.control_outline, "", Vec2{size.x + 2, size.y + 2}});

    // Adjust cursor position for subsequent controls, considering the toggle button's size and spacing.
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x + size.x + global_config.item_spacing, cursor_pos.y});
    utils::misc::push_cursor_pos(Vec2{cursor_pos.x, cursor_pos.y + size.y / 2 + global_config.item_spacing});

    utils::misc::push_font(font);
}
