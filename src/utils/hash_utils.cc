//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Splits a string into a vector of strings based on a specified separator character.
 * @param str The input string to be split.
 * @param separator The character used to split the input string.
 * @return A vector of substrings split from the input string.
 */
std::vector<std::string> mgui::utils::hash::split_str(const char* str, const char separator) {
    std::vector<std::string> output;
    std::string substring;
    std::istringstream stream{str};

    while (std::getline(stream, substring, separator)) {
        output.push_back(substring);
    }

    return output;
}

/**
 * @brief Recursively calculates a hash value for a given string using a specific hashing algorithm.
 * @param str The input string to hash.
 * @param value The initial hash value or the current hash value in recursive calls (default is 0x811c9dc5).
 * @return The calculated hash value for the input string.
 */
uint32_t mgui::utils::hash::hash(const char* str, const uint32_t value) {
    // The magic number 0x1000193ull is part of the hashing algorithm, used to create a unique hash value.
    return *str ? hash(str + 1, (value ^ *str) * 0x1000193ull) : value;
}