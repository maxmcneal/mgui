//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Pushes a cursor position onto the GUI window context's stack.
 * @param pos The cursor position to push.
 */
void mgui::utils::misc::push_cursor_pos(const mgui::Vec2 pos) {
    window_ctx.cursor_positions.push(pos);
}

/**
 * @brief Pops the top cursor position off the GUI window context's stack.
 * @return The top cursor position.
 */
mgui::Vec2 mgui::utils::misc::pop_cursor_pos() {
    const mgui::Vec2 pos = window_ctx.cursor_positions.top();
    window_ctx.cursor_positions.pop();
    return pos;
}

/**
 * @brief Pushes a font onto the GUI window context's font stack.
 * @param font The font to push.
 */
void mgui::utils::misc::push_font(const unsigned long font) {
    globals::window_ctx.fonts.push(font);
}

/**
 * @brief Pops the top font off the GUI window context's font stack.
 * @return The top font.
 */
unsigned long mgui::utils::misc::pop_font() {
    const unsigned long font = window_ctx.fonts.top();
    window_ctx.fonts.pop();
    return font;
}
