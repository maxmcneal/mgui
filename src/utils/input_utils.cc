//@packer:ignore
#include "../mgui.hh"

using namespace mgui::globals;
//@packer:resume

/**
 * @brief Starts the input loop by polling inputs based on the window name.
 * @param window_name The name of the window to poll input from.
 * @exception Throws std::exception if no window name is provided.
 */
void mgui::poll_input(std::string_view window_name) {
    if (window_name.empty())
        throw std::exception("No window from where input should be read from specified in function parameter.");

    for (int i = 0; i < 256; i++) {
        prev_key_state[i] = key_state[i];
        key_state[i] = GetAsyncKeyState(i) & 0x8000;
    }

    POINT p_mouse_pos;
    GetCursorPos(&p_mouse_pos);
    ScreenToClient(FindWindow(nullptr, window_name.data()), &p_mouse_pos);
    previous_mouse_pos = mouse_pos;
    mouse_pos = Vec2{static_cast<float>(p_mouse_pos.x), static_cast<float>(p_mouse_pos.y)};

    if (!input_loop_started)
        input_loop_started = true;
}

/**
 * @brief Starts the input loop by polling inputs based on the window handle.
 * @param hwnd The handle of the window to poll input from.
 * @exception Throws std::exception if no window handle is provided.
 */
void mgui::poll_input(HWND hwnd) {
    if (!hwnd)
        throw std::exception("No window from where input should be read from specified in function parameter.");

    for (int i = 0; i < 256; i++) {
        prev_key_state[i] = key_state[i];
        key_state[i] = GetAsyncKeyState(i) & 0x8000;
    }

    POINT p_mouse_pos;
    GetCursorPos(&p_mouse_pos);
    ScreenToClient(hwnd, &p_mouse_pos);
    previous_mouse_pos = mouse_pos;
    mouse_pos = Vec2{static_cast<float>(p_mouse_pos.x), static_cast<float>(p_mouse_pos.y)};

    if (!input_loop_started)
        input_loop_started = true;
}

/**
 * @brief Checks if a specific key was pressed since the last poll.
 * @param key The key code to check.
 * @return True if the key was pressed, false otherwise.
 */
bool mgui::utils::input::key_pressed(const int key) {
    return key_state[key] && !prev_key_state[key];
}

/**
 * @brief Checks if a specific key is currently down.
 * @param key The key code to check.
 * @return True if the key is down, false otherwise.
 */
bool mgui::utils::input::key_down(const int key) {
    return key_state[key];
}

/**
 * @brief Checks if a specific key was released since the last poll.
 * @param key The key code to check.
 * @return True if the key was released, false otherwise.
 */
bool mgui::utils::input::key_released(const int key) {
    return !key_state[key] && prev_key_state[key];
}

/**
 * @brief Checks if the mouse is within a specified region.
 * @param x The x-coordinate of the top-left corner of the region.
 * @param y The y-coordinate of the top-left corner of the region.
 * @param w The width of the region.
 * @param h The height of the region.
 * @return True if the mouse is within the region, false otherwise.
 */
bool mgui::utils::input::mouse_in_region(const int x, const int y, const int w, const int h) {
    return mouse_pos.x > x && mouse_pos.y > y && mouse_pos.x < x + w && mouse_pos.y < y + h;
}