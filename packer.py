import glob
import os
import re
import shutil


def yes_no(question_to_be_answered):
  while True:
    choice = input(question_to_be_answered).lower()
    if choice[:1] == 'y':
      return True
    elif choice[:1] == 'n':
      return False
    else:
      print("Please respond with 'Y' for yes or 'N' for no.\n")


print("""\n
           _       _                 _               _ 
 _ __ ___ (_)_ __ (_)_ __ ___   __ _| |   __ _ _   _(_)
| '_ ` _ \| | '_ \| | '_ ` _ \ / _` | |  / _` | | | | |
| | | | | | | | | | | | | | | | (_| | | | (_| | |_| | |
|_| |_| |_|_|_| |_|_|_| |_| |_|\__,_|_|  \__, |\__,_|_|
                                         |___/           
""")

generated_file_comment = """
//            _       _                 _               _ 
//  _ __ ___ (_)_ __ (_)_ __ ___   __ _| |   __ _ _   _(_)
// | '_ ` _ \| | '_ \| | '_ ` _ \ / _` | |  / _` | | | | |
// | | | | | | | | | | | | | | | | (_| | | | (_| | |_| | |
// |_| |_| |_|_|_| |_|_|_| |_| |_|\__,_|_|  \__, |\__,_|_|
//                                          |___/           
//  - max mcneal
"""

ignored_files = []
output_contents = []

proceed = yes_no("  Do you want to pack everything into a singular header file? (Y/N) ")
if proceed:
  output_contents.append(generated_file_comment)
  output_contents.append("#include \"./mgui.hh\"")
  output_contents.append("using namespace mgui::globals;")
  output_contents.append("mgui::Functions mgui::functions;")
  output_contents.append("mgui::GuiWindowContext mgui::globals::window_ctx;")

  for filename in glob.iglob('src/**/*.cc', recursive=True):
    if os.path.split(filename)[1] not in ignored_files:
      with open(filename, 'r') as file:
        output_contents.append(re.sub(r'(?s)^//@packer:ignore.*//@packer:resume$', '', file.read(), flags=re.M))

  if not os.path.exists('dist'):
    os.mkdir('dist')

  shutil.copy('src/mgui.hh', 'dist/mgui.hh')
  with open('dist/mgui.cc', 'w') as output:
    output.write('\n'.join(output_contents))

  print("\nSuccessfully packed. See /dist folder for output.\n")
else:
  quit()
