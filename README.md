<div align="center">
<img width="258" src="images/header.png" alt="minimal gui logo" />
<h4>Very minimal immediate mode graphical user interface framework.</h4>

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-c-plus-plus.svg)](https://forthebadge.com)
</div>

# 

Simple, single-header immediate mode graphic user interface framework written in c++20. Completely universal, compatible with all graphic APIs: DirectX, OpenGL, Vulkan, etc. Intended to be completely dependency free.

### Installation
1. Clone the repository. ( `git clone https://gitlab.com/laoganma/minimal-gui.git` )
2. Open clone directory. ( `cd minimal-gui` )
3. Run the packer.py script to pack all files into single header. ( `py packer.py` )
4. Copy files from the /dist in the base directory to embed the framework in your project.

### Embedding
This framework requries you to create proxy functions somewhere in your code, then override it's rendering functions with yours.

```cpp
mgui::functions.draw_line = [](int x, int y, int x2, int y2, mgui::Color c) noexcept {
    /* draw line using your renderer */
};

mgui::functions.draw_rect = [](int x, int y, int x2, int y2, mgui::Color c) noexcept {
    /* draw outlined rect using your renderer */
};

mgui::functions.draw_filled_rect = [](int x, int y, int x2, int y2, mgui::Color c) noexcept {
    /* draw filled rect using your renderer */
};

mgui::functions.draw_text = [](int x, int y, mgui::Color color, int font, bool center, const char* text) noexcept {
    /* draw text using your renderer */
};

mgui::functions.get_text_size = [](unsigned long font, const char* text, int& width, int& height) noexcept {
 /* calculate text size here */ 
};

mgui::functions.get_frametime = []() noexcept -> float {
 /* calculate text size here */ 
};
```

### Implementation
```cpp
// Start input loop
mgui::poll_input("UnityWndClass");

static bool example = false;
static int example_int = 10;
if (mgui::begin_window("mgui example window", { 500, 350 }, g::Tahoma, zgui::zgui_window_flags_none))
{
    mgui::checkbox("sample checkbox #checkbox_hash", example);
    mgui::slider_int("example slider #you_cant_see_this_hash", 0, 40, example_int);
    mgui::end_window();
}
```

</div>